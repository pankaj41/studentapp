using Microsoft.EntityFrameworkCore;
using Student.API.Models;

namespace Student.API.Data
{
    public class DataContext : DbContext
    {
        
        public DataContext(DbContextOptions<DataContext> options) : base(options){ }

        public DbSet<GradStudent> GradStudents { get; set; }
    }
}