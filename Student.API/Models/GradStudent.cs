namespace Student.API.Models
{
    public class GradStudent
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}